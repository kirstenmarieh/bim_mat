import plotly.express as px
import numpy as np
import plotly.graph_objects as go
import pandas as pd
from datetime import datetime
from Bio import Entrez
import ssl
import json
import copy
import dash
import dash_core_components as dcc
import dash_html_components as html

def unify_date(da):
    '''takes in date dictionary {pmid:str(date)} and returns a dictionary with PMIDs as keys and datetime objects as values.
    If the given string date includes year, month, and date, then that datetime object is returned and unaltered. If
    the value contains on the year and month, '1' is assigned as the day by default. If the publication has only a year,
    month and day are set to 1 by default. If the date is unavailable, the date is assigned as July 4th 2021 by default.
    '''

    for k in da.keys():
        str_val = da[k] #string date
        try: #year month day present
            date_time_obj = datetime.strptime(str_val,'%Y %b %d')
            da[k] = date_time_obj.date()
        except:
            try:
                date_time_obj = datetime.strptime(str_val, '%Y %b') #year and month available
                da[k] = date_time_obj.date()
            except:
                try:
                    date_time_obj = datetime.strptime(str_val, '%Y') #only year available
                    da[k] = date_time_obj.date()
                except: #no date info available
                    date_time_obj = datetime(2021, 7, 4)
                    da[k] = date_time_obj.date()
    return da

def create_concdict(pub_rep):
    '''creates and returns a dictionary with bioconcept id's as keys and bioconcept strings as values. Takes in the publication representation.'''
    conc_di = {}
    for entry in pub_rep:
        for j in range(len(entry['header']['bioconcepts']['bioconcept'])):
            conc_di[entry['header']['bioconcepts']['bioconcept'][j]]=entry['header']['bioconcepts']['bioconcept_identifier'][j]
        for k in entry['abstract']['bioconcepts']:
            try:
                for j in range(len(entry['abstract']['bioconcepts'][k]['bioconcept'])):
                    conc_di[entry['abstract']['bioconcepts'][k]['bioconcept'][j]] = entry['abstract']['bioconcepts'][k]['bioconcept_identifier'][j]
            except KeyError:
                continue

    new_di = {} # intermediate dictionary to remove duplicate values and keep only the one with shortest length
    for key, val in conc_di.items():
        new_di.setdefault(val, set()).add(key)

    for key, val in new_di.items():
        if len(val) > 1: #duplicate values exist
            temp_list = list(val) # turn sets to lists
            new_di[key] = min(temp_list, key=len) # find the minumum and replace the values with it
        else:
            new_di[key] = ''.join(val) # if length was 1, turn the set to a string
    conc_dict = {value: key for (key, value) in new_di.items()} # invert the dictionary
    #print(conc_dict)
    return conc_dict

def create_type_dict(pub_rep, conc_dict):
    '''creates and returns a dictionary with bioconcept strings as keys and bioconcept entity type as values. Takes in the publication representation and the
    dictionary with bioconcept ids as values and bioconcept strings as keys.'''
    entity_di={}
    for key in conc_dict.keys():
        target = key #target bioconcept string
        for entry in pub_rep:
            for k in entry['abstract']['bioconcepts']:
                try:
                    for j in range(len(entry['abstract']['bioconcepts'][k]['bioconcept'])):
                        if entry['abstract']['bioconcepts'][k]['bioconcept'][j]==target: #if concepts match
                            entity_di[entry['abstract']['bioconcepts'][k]['bioconcept_identifier'][j]]=entry['abstract']['bioconcepts'][k]['entities'][j][2]
                except KeyError:
                    continue
    return entity_di

def create_df(pub_rep):
    '''takes in the publication representation
    and returns a pandas dataframe with the information
    contained in the bioconcept indicator matrix.'''

    conc_dict = create_concdict(pub_rep) #dict with concept string as keys and id as values
    entity_di = create_type_dict(pub_rep, conc_dict)

    bio_string_col = list(conc_dict.keys()) #column for conc strings
    bio_id_col = list(conc_dict.values()) #column for conc ids

    year_di = {}
    day_di = {}
    month_di = {}
    date_di = {}
    #for entry in pub_rep:
    for v in conc_dict.values():
        year_di[v]=[]
        day_di[v] = []
        month_di[v]=[]
        date_di[v]=[]
        for entry in pub_rep:
            if v in entry['header']['bioconcepts']['bioconcept_identifier']:
                year_di[v].append(entry['header']['pub_year'])
                date_di[v].append(entry['header']['pub_date'])
            else:
                pass
            for j in entry['abstract']['bioconcepts']:
                try:
                    #for k in range(len(entry['abstract']['bioconcepts'][j]['bioconcept'])):

                    if v in entry['abstract']['bioconcepts'][str(j)]['bioconcept_identifier']:
                        year_di[v].append(entry['header']['pub_year'])
                        date_di[v].append(entry['header']['pub_date'])
                except KeyError:
                    pass

    first_year_col = []
    first_date_col = []
    for v in conc_dict.values():
        if v in date_di.keys():
            #sort and take first year
            first_year_col.append(sorted(year_di[v])[0])
            first_date_col.append(sorted(date_di[v])[0])

    entity_list = []
    for k in bio_id_col:
        if k in entity_di.keys():
            entity_list.append(entity_di[k])
        else:
            entity_list.append('') #something is wrong, this list length is inconsistent without the empty string.. (length 35 or 36, but should be 37)

    bio_type_col = entity_list #column for conc type

    col_arrays=[bio_id_col,
                bio_string_col,bio_type_col,[d.year for d in first_date_col], [d.month for d in first_date_col], [d.day for d in first_date_col]] #will be conc id, conc, conc class, pub year values
    col_tups = list(zip(*col_arrays))
    col_headers = pd.MultiIndex.from_tuples(col_tups, names=["Concept ID", "Concept", "Concept Class", "First Year", "First Month", "First Day"]) #multiindex columns

    row_arrays = [[entry['id']['pub_id'] for entry in pub_rep], [entry['header']['pub_date'].year for entry in pub_rep],
                  [entry['header']['pub_date'].month for entry in pub_rep],[entry['header']['pub_date'].day for entry in pub_rep]] #+[0]*5
    row_tups = list(zip(*row_arrays))
    row_headers = pd.MultiIndex.from_tuples(row_tups, names = ['pmid', 'pub year', 'pub month', 'pub day']) #multiindex rows

    bin_list = []
    for i, p in enumerate(pub_rep): #pub rep and column length will be same!
        bin_sublist = [] #list of zeros and 1s for each publication. Entry corresponds to a bioconcept, 0 if not found 1 if found
        if row_headers.get_level_values(0)[i] == p['id']['pub_id']: #pmid found
            for j in col_headers.get_level_values(0): #bioconc ids
                found = 0 #conc not found yet
                if j in p['header']['bioconcepts']['bioconcept_identifier']: #if bioconcept id exists in header
                    found = 1
                else:
                    found = 0
                if found == 0: #conc not found in title, try sentences
                    for k in range(len(p['abstract']['sentences'])):
                        try:
                            if j in p['abstract']['bioconcepts'][str(k)]['bioconcept_identifier']: #if bioconcept id exists in abstract sentences
                                found = 1
                                break
                            else:
                                found = 0
                        except KeyError:
                            pass
                bin_sublist.append(found)
            bin_list.append(bin_sublist)

    data = [item for sublist in bin_list for item in sublist]

    df = pd.DataFrame(np.array(data).reshape((len(row_headers),len(col_headers))),
                      index=row_headers, #was row_headers.... hm....
                      columns=col_headers)

    df = df.sort_values(['pub year', 'pub month', 'pub day'], ascending = [True, True, True]) #sort df by pub year, pub month, pub day ROW
    df = df.sort_index(axis=1, level=[3,4,5], ascending=[True,True,True]) #sort by first year in COLUMNS
    y_labels = list(row_headers.get_level_values(0))
    y_labels = [str(y) for y in y_labels]

    return df, y_labels

def create_heatmap_lists(df):
    '''takes in the multi indexed dataframe containing only publication information and returns lists to create the
    binary matrix, a list of days, months, years, concept type, concept id, concept string,
    binary nested list (0 or 1 if a concept occurs in a publication), frequency list (number of publications a concept shows up in),
    and a nested list of colors corresponding to the frequencies.'''
    day_list = []
    month_list = []
    year_list = []
    conc_type_list = []
    conc_id_list = []
    conc_str_list = []
    bin_list = []
    freq_list = []
    corr_freq_list=[]
    year_df_ls = list(df.columns.get_level_values(3))
    concstr_df_ls = list(df.columns.get_level_values(1))
    concid_df_ls = list(df.columns.get_level_values(0))
    conctype_df_ls = list(df.columns.get_level_values(2))
    day_df_ls = list(df.columns.get_level_values(4))
    month_df_ls = list(df.columns.get_level_values(5))

    for i in range(len(df)): #concs in each pmid
        day_sl = []
        month_sl = [] #append the year of the bioconcept if corresponding entry is a 1, else append ''
        year_sl = []
        conc_id_sl = []
        conc_str_sl = []
        conc_type_sl = []

        bin_list.append(list(df.iloc[i])) #binary values per pub
        for j, k in enumerate(list(df.iloc[i])):#iterates over rows

            # if k == 0:
            #     year_sl.append('')
            #     conc_id_sl.append('')
            #     conc_type_sl.append('')
            #     conc_str_sl.append('')
            #     day_sl.append('')
            #
            # else:
            year_sl.append(year_df_ls[j])
            month_sl.append(month_df_ls[j])
            day_sl.append(day_df_ls[j])
            conc_id_sl.append(concid_df_ls[j])
            conc_type_sl.append(conctype_df_ls[j])
            conc_str_sl.append(concstr_df_ls[j])

        year_list.append(year_sl)
        month_list.append(month_sl)
        day_list.append(day_sl)
        conc_id_list.append(conc_id_sl)
        conc_type_list.append(conc_type_sl)
        conc_str_list.append(conc_str_sl)
        freq_list.append([sum(list(df.iloc[i]))]*len(df.iloc[i])) #number of total bioconcepts per pub

    for k in list(df.columns.get_level_values(0)):

        nd_list = df[k].values.tolist() #list of lists of column values
        flat_list = [item for sublist in nd_list for item in sublist]
        corr_freq_list.append(sum(flat_list))

    color_corr_freq_list = copy.deepcopy(corr_freq_list)

    for i in range(len(color_corr_freq_list)):
        color_corr_freq_list[i] = int(round(color_corr_freq_list[i]/len(df),2)*100)

    sparse_freq_list = copy.deepcopy(freq_list)#freq_list.copy() #sparse freq list for coloring
    sparse_text_freq_list = copy.deepcopy(freq_list)
    for i in range(len(bin_list)):
        for j in range(len(bin_list[i])):
            if bin_list[i][j] == 0:
                sparse_freq_list[i][j] = 0
                sparse_text_freq_list[i][j] = ''
            else: #convert count to percentage
                sparse_freq_list[i][j]= int((round(sparse_freq_list[i][j] / len(df.columns.get_level_values(0)),2)) * 100) #num concept in pub/ tot num concepts  #int(round(sparse_freq_list[i][j] / len(df), 2)*100) #sparse_freq_list[i][j]#(round(sparse_freq_list[i][j] / len(df.columns.get_level_values(0)),2))
    return year_list, month_list, day_list, conc_id_list, conc_type_list, conc_str_list, freq_list, sparse_freq_list, corr_freq_list, color_corr_freq_list, sparse_text_freq_list

def complete_df(df):
    '''takes in the multiindexed dataframe and adds the column indices to the df at the top, returns the altered df.'''
    col_df = df.columns.to_frame()
    transpose_col_df = col_df.transpose()
   # df_with_info =df.append(transpose_col_df, ignore_index=True)
    df_with_info = pd.concat([transpose_col_df, df], ignore_index=True)
    return df_with_info#df_with_info

def create_colorlist(df_with_info, sparse_freq_list):
    '''creates a nested list of color values to be displayed from 0-1.'''
    color_list = copy.deepcopy(sparse_freq_list)
    for i in range(len(color_list)):
        for j in range(len(color_list[i])):
            if i == 4:#len(pub_rep): #frequency column, SCALE THESE VALUES
                color_list[i][j] = round(((color_list[i][j]/100)*80)+20)
            elif i < 5: #if i is NOT a publication, then it is a date or bioconcept
                if type(color_list[i][j]) == int: #day, year, month
                    color_list[i][j] = 0 #white for now
                else: #type is string, i.e. bioconcept id or type, etc
                    color_list[i][j] = 0 #white for now

            else: #frequency stays the same
                if df_with_info.iloc[2][j] == 'Gene' and color_list[i][j] != 0: #bioconcept identifier colors as colors
                    color_list[i][j]=10
                elif df_with_info.iloc[2][j] == 'Disease' and color_list[i][j] != 0:
                    color_list[i][j] = 11
                elif df_with_info.iloc[2][j] == 'Mutation' and color_list[i][j] != 0:
                    color_list[i][j] = 12
                elif df_with_info.iloc[2][j] == 'Species' and color_list[i][j] != 0:
                    color_list[i][j] = 13
                elif df_with_info.iloc[2][j] == 'Chemical' and color_list[i][j] != 0:
                    color_list[i][j] = 14
                elif df_with_info.iloc[2][j] == 'CellLine' and color_list[i][j] != 0:
                    color_list[i][j] = 15
                elif color_list[i][j] != 0 and df_with_info.iloc[2][j] == '': #no bioconcept type exists, default to something else
                    color_list[i][j] = 16
    return color_list

def update_sparse_lists(sparse_freq_list, sparse_text_freq_list, color_corr_freq_list, corr_freq_list, df_with_info):
    '''alters the lists to be used by the heatmap so the display is correct.'''
    num_additional_rows = df_with_info.columns.nlevels #currently 6 column values added

    sparse_freq_list.insert(0, list(df_with_info.iloc[num_additional_rows-1]))#5
    sparse_freq_list.insert(1, list(df_with_info.iloc[num_additional_rows-2])) #4
    sparse_freq_list.insert(2, list(df_with_info.iloc[num_additional_rows-3])) #3
    sparse_freq_list.insert(3, list(df_with_info.iloc[num_additional_rows-4])) #2
    #sparse_freq_list.insert(4, list(df_with_info.iloc[num_additional_rows-5])) #1
    #sparse_freq_list.insert(5, list(df_with_info.iloc[num_additional_rows-6])) #0
    sparse_freq_list.insert(4, color_corr_freq_list)

    sparse_text_freq_list.insert(0, list(df_with_info.iloc[num_additional_rows-1])) #list(df_with_info.iloc[5])
    sparse_text_freq_list.insert(1, list(df_with_info.iloc[num_additional_rows-2]))
    sparse_text_freq_list.insert(2, list(df_with_info.iloc[num_additional_rows-3]))
    sparse_text_freq_list.insert(3, list(df_with_info.iloc[num_additional_rows-4]))
    #sparse_text_freq_list.insert(4, list(df_with_info.iloc[num_additional_rows-5]))
    #sparse_text_freq_list.insert(5, list(df_with_info.iloc[num_additional_rows-6]))
    sparse_text_freq_list.insert(4, corr_freq_list)
    return sparse_freq_list, sparse_text_freq_list


#fname = '/Users/Kirsten/Desktop/archimedes-system/data/searches/thermostable_vaccine_sars_cov2w_13_2021_11_06_17_16_43.json'
fname = 'thermostable_vaccine_covid_13_2021_11_11_11_31_46.json'
with open(fname, 'r') as f:
    pub_rep = json.load(f)

pmid_list = [entry['id']['pub_id'] for entry in pub_rep]
Entrez.email = "kayttaytya@web.de"
ssl._create_default_https_context = ssl._create_unverified_context
da = {}
for i, p in enumerate(pmid_list):
    handle = Entrez.esummary(db="pubmed", id=p)
    record = Entrez.read(handle)
    handle.close()

    if len(record):
        da[p] = record[0]["PubDate"]
    else:
        da[p] = ['0 Dec 12']

date_dict = unify_date(da)

for k in date_dict.keys():
    for entry in pub_rep:
        if entry['id']['pub_id'] == k:
            entry['header']['pub_date']=date_dict[k] #add date to publication representation
            entry['header']['pub_day'] = date_dict[k].day
            entry['header']['pub_month'] = date_dict[k].month

#create_concdict(pub_rep)
df, y_labels = create_df(pub_rep)
year_list, month_list, day_list, conc_id_list, conc_type_list, conc_str_list, freq_list, sparse_freq_list, corr_freq_list, color_corr_freq_list, sparse_text_freq_list = create_heatmap_lists(df) #returns year_list, month_list, day_list, conc_id_list, conc_type_list, conc_str_list, freq_list
df_with_info = complete_df(df)

pmids = []
for y in y_labels:
    pmids.append([y]*len(list(df_with_info.columns.get_level_values(0))))

y_labels.insert(0, 'First Day')
y_labels.insert(1, 'First Month')
y_labels.insert(2, 'First Year')
y_labels.insert(3, 'Type')
#y_labels.insert(4, 'String')
#y_labels.insert(5, 'ID')
y_labels.insert(4, 'Frequency')


sparse_freq_list, sparse_text_freq_list = update_sparse_lists(sparse_freq_list, sparse_text_freq_list, color_corr_freq_list, corr_freq_list, df_with_info)
color_list = create_colorlist(df_with_info, sparse_freq_list)


colorscale=[[0.0, 'rgb(255,255,255)'], [.1, 'rgb(153,50,204)'],
            [.11, 'rgb(255,165,0)'], [.12, 'rgb(139,69,19)'],
            [.13, 'rgb(173,216,230)'],[.14, 'rgb(34,139,34)'],
            [.15, 'rgb(72,209,204)'], [.16, 'rgb(255,192,203)'],
            [.2, 'rgb(255,0,0)'], [.6, 'rgb(255,255,0)'], [1.0, 'rgb(0,128,0)']]

x_vals=[]
for i in range(len(df_with_info.columns.get_level_values(0))):
    conc = df_with_info.iloc[1][i].split(' ')
    x_vals.append(conc[0] + ' '+ str(df_with_info.iloc[4][i])+ '/'+str(df_with_info.iloc[5][i])+'/'+str(df_with_info.iloc[3][i])) #df_with_info.iloc[1][i]

for i in range(5): #pad lists for hovertext, must be 5 because 5 rows were added to the df for the multiindex info
    conc_str_list.insert(i,['']*len(df_with_info.columns.get_level_values(0)))
    conc_id_list.insert(i,['']*len(df_with_info.columns.get_level_values(0)))
    conc_type_list.insert(i,['']*len(df_with_info.columns.get_level_values(0)))
    year_list.insert(i,['']*len(df_with_info.columns.get_level_values(0)))
    pmids.insert(i,['']*len(df_with_info.columns.get_level_values(0)))

fig = px.imshow(color_list, color_continuous_scale=colorscale, aspect="auto", #was 'BuPu, sparse_freq_list colorscale_list
                title='BIM', y=y_labels, x = x_vals) #was y_labels

fig.update_traces(
    text=sparse_text_freq_list, texttemplate="%{text}", textfont_size=12, #was freq_list
    customdata=np.moveaxis([conc_str_list, conc_id_list, conc_type_list, year_list, pmids], 0,-1),
    hovertemplate="%{customdata[0]}<br>Concept ID: %{customdata[1]}<br>Concept Type: %{customdata[2]}<br>Year: %{customdata[3]}<br> PMID: %{customdata[4]}<extra></extra>"
)
fig.update_xaxes(visible=True, side='top')
fig.update_yaxes(visible=True)
fig.update_coloraxes(showscale=False)

fig.show()
fig = go.Figure() # or any Plotly Express function e.g. px.bar(...)

app = dash.Dash()
app.layout = html.Div([
    dcc.Graph(figure=fig),
])

app.run_server(debug=True, use_reloader=False)

